<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable = ['user_id', 'recurso_id', 'tipo', 'quantidade'];
}
