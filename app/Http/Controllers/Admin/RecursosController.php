<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Recurso;
use App\Atividade;

class RecursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbList = json_encode([
            [
                "text" => "Home",
                "href" => route('home')
            ],
            [
                "text" => "Artigos",
                "active" => true
            ]
        ]);

        $recursos = (Recurso::select('id', 'descricao', 'quantidade', 'observacao')->get());

        $recursosList = array();
        foreach($recursos as $value) {
            $atividade = array(
                "id" => $value['id'],
                "descricao" => $value['descricao'],
                "quantidade" => $value['quantidade'],
                "observacao" => $value['observacao'],
                "status" => ($value['quantidade'] > 0 ? "Recurso Disponível" : "Recurso Indisponível")
            );
            array_push($recursosList, $atividade);
        }
        $recursosList = json_encode($recursosList);

        return view(
            'admin.recursos.index',
            compact('breadcrumbList', 'recursosList')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbList = json_encode([
            [
                "text" => "Home",
                "href" => route('home')
            ],
            [
                "text" => "Recursos",
                "href" => route('recursos.index')
            ],
            [
                "text" => "Novo",
                "active" => true
            ]
        ]);
        return view(
            'admin.recursos.create',
            compact('breadcrumbList')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validation = Validator::make($data, [
            'descricao' => 'required',
            'observacao' => 'required',
            'quantidade' => 'required',
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        
        Recurso::create($data);

        return redirect()->route('recursos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recurso = Recurso::find($id);

        $breadcrumbList = json_encode([
            [
                "text" => "Home",
                "href" => route('home')
            ],
            [
                "text" => "Recursos",
                "href" => route('recursos.index')
            ],
            [
                "text" => "Editar",
                "active" => true
            ]
        ]);
        return view(
            'admin.recursos.create',
            compact('breadcrumbList', 'recurso')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if(isset($data["tipo"])){
            $quantidade = $data["quantidade"];
            $recurso = Recurso::find($id);

            $atividade = new Atividade();
            $atividade->user_id = \Auth::user()->id;
            $atividade->recurso_id = $id;
            $atividade->quantidade = $quantidade;
            
            if($data["tipo"] == "entrada") {
                $atividade->tipo = 1;
                $recurso->quantidade += $quantidade;
                $atividade->save();
            } else if ($data["tipo"] == "saida") {
                if($recurso->quantidade >= $quantidade) {
                    $recurso->quantidade -= $quantidade;
                    $atividade->tipo = 2;
                    $atividade->save();
                }
            }
            $recurso->save();
        } else {
            $validation = Validator::make($data, [
                'descricao' => 'required',
                'observacao' => 'required',
            ]);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            
            Recurso::find($id)->update($data);
        }

        return redirect()->route('recursos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Recurso::find($id)->delete();

        return response()->json(['success' => 'success'], 204);
    }
}
