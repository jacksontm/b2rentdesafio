<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Atividade;
use App\User;
use App\Recurso;

class AtividadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbList = json_encode([
            [
                "text" => "Home",
                "href" => route('home')
            ],
            [
                "text" => "Atividades",
                "active" => true
            ]
        ]);

        $atividades = (Atividade::select('id', 'tipo', 'quantidade', 'user_id', 'recurso_id')->get());


        $atividadesList = array();
        foreach($atividades as $value) {
            $atividade = array(
                "id" => $value['id'],
                "tipo" => $value['tipo'],
                "quantidade" => $value['quantidade'],
                "user" => User::find($value['user_id'])->name,
                "recurso" => Recurso::find($value['recurso_id'])->descricao
            );
            array_push($atividadesList, $atividade);
        }
        $atividadesList = json_encode($atividadesList);

        return view(
            'admin.atividades.index',
            compact('breadcrumbList', 'atividadesList')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
