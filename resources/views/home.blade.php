@extends('layouts.app') 

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
            <b-breadcrumb :items="{{$breadcrumbList}}"></b-breadcrumb>
			<panel-component
                title="Dashboard"
                color="dark">
				<div class="row">
					<div class="col-md-4">
                        <card-info-component 
                            title="Recursos"
                            color="danger"
                            count="10"
                            url="{{ route('recursos.index') }}"
                            icon="ion ion-settings">
						</card-info-component>
					</div>
					<div class="col-md-4">
                        <card-info-component 
                            title="Atividades"
                            color="info"
                            count="10"
                            url="{{ route('atividades.index') }}"
                            icon="ion ion-navicon-round">
						</card-info-component>
					</div>
				</div>
			</panel-component>
		</div>
	</div>
</div>
@endsection