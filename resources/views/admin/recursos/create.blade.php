@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<b-breadcrumb :items="{{$breadcrumbList}}"></b-breadcrumb>
			<panel-component title="Novo recurso" color="dark">
				<form-component method="{{empty($recurso) ? 'POST' : 'PUT'}}" action="{{empty($recurso) ? route('recursos.store') : route('recursos.update', $recurso->id) }}" token="{{ csrf_token() }}">
					<div class="form-group">
						<label for="descricaoInput">Descrição</label>
						<input type="text" name="descricao" class="form-control" id="descricaoInput" value="{{ old('descricao', (!empty($recurso) ? $recurso->descricao : '')) }}"
						 placeholder="Digite a descrição">
						@if ($errors->has('descricao'))
						<label for="descricaoInput" class="form-text small text-danger">{{$errors->first('descricao')}}</label>
						@endif
					</div>
                    @if(empty($recurso))
                    <div class="form-group">
						<label for="quantidadeInput">Quantidade</label>
						<input type="number" name="quantidade" class="form-control" id="quantidadeInput" value="{{ old('quantidade', (!empty($recurso) ? $recurso->quantidade : '')) }}"
						 placeholder="Digite a quantidade">
						@if ($errors->has('quantidade'))
						<label for="quantidadeInput" class="form-text small text-danger">{{$errors->first('quantidade')}}</label>
						@endif
					</div>
                    @endif
					<div class="form-group">
						<label for="observacaoTextarea">Observação</label>
						<textarea name="observacao" class="form-control" id="observacaoTextarea" rows="3" placeholder="Observação...">{{ old('observacao', (!empty($recurso) ? $recurso->observacao : '')) }}</textarea> 
						@if ($errors->has('observacao'))
						<label for="observacaoTextarea" class="form-text small text-danger">{{$errors->first('observacao')}}</label>
						@endif
					</div>
					<button type="submit" class="btn btn-primary btn-block">Enviar</button>
				</form-component>
			</panel-component>
		</div>
	</div>
</div>
@endsection