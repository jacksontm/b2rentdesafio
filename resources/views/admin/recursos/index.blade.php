@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<b-breadcrumb :items="{{ $breadcrumbList }}"></b-breadcrumb>
			<panel-component title="Lista de recursos" color="dark">
                <table-page
                    v-bind:titles="['#','Descrição', 'Quantidade', 'Observação', 'Status']"
                    v-bind:items="{{ $recursosList }}"
					url="{{route('recursos.index')}}"
					token="{{ csrf_token() }}"
					estoque="true"></table-page>
			</panel-component>
		</div>
	</div>
</div>
@endsection