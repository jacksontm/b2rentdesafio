@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<b-breadcrumb :items="{{ $breadcrumbList }}"></b-breadcrumb>
			<panel-component title="Lista de atividades" color="dark">
                <table-page
                    v-bind:titles="['#','Tipo', 'Quantidade', 'Usuário', 'Recurso']"
                    v-bind:items="{{ $atividadesList }}"></table-page>
			</panel-component>
		</div>
	</div>
</div>
@endsection